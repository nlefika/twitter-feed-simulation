# Twitter Feed Simulator
------------------------
Problem Description
------------------------

Program that simulates a twitter-like feed. The program takes in two input files specified when running the application 
with the content as indicated below. 
<br/>
**Here is an example. Given user file named user.txt:** <br/>
Ward follows Alan <br/>
Alan follows Martin <br/>
Ward follows Martin, Alan <br/>

**And given tweet file named tweet.txt:** <br/>
Alan> If you have a procedure with 10 parameters, you probably missed some. <br/>
Ward> There are only two hard things in Computer Science: cache invalidation, naming things and off-by-1 errors. <br/>
Alan> Random numbers should not be generated with a method chosen at random. <br/>

--------------------------
Running the Application
--------------------------
The application is written in Java. To build it, you will need maven.

```
$ mvn install
```

The build folder in this case is the default `target` folder. Run the application pointing to the jar

```
$ java -jar  -Dlog4j.configuration=file:src/main/resources/log4j.properties target/twitter-feed-simulation-1.0-SNAPSHOT.jar user.txt tweet.txt
```

--------------------------
Assumptions
--------------------------

- The application assumes the format of the file is valid as per the description above. It will not check for a malformed file.
- The tweets printed in the description will appear ordered in a natural ascending fashion. The printed example does not appear to be ordered on the tweets.
- 70 percent test coverage is sufficient as per the [image](Screenshot%20from%202020-02-13%2012-59-53.png)