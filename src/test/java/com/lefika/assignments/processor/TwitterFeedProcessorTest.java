package com.lefika.assignments.processor;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

public class TwitterFeedProcessorTest {


    private TwitterFeedProcessor twitterFeedProcessor;

    @Before
    public void setUp() throws Exception {
        BufferedReader bufferedReader = Mockito.mock(BufferedReader.class);
        when(bufferedReader.lines())
                .thenReturn(Stream.of("Ward follows Alan", "Alan follows Martin", "Ward follows Martin, Alan"));
        twitterFeedProcessor = new TwitterFeedProcessor(new HashMap<>());
        twitterFeedProcessor.processUserFile("user.txt");
    }

    @Test
    public void processUserFile() {
        assertFalse(twitterFeedProcessor.getAllUsers().isEmpty());
        assertEquals(3, twitterFeedProcessor.getAllUsers().size());
        assertTrue(twitterFeedProcessor.getAllUsers().get("Martin").getFollowing().isEmpty());
        assertFalse(twitterFeedProcessor.getAllUsers().get("Alan").getFollowing().isEmpty());
        assertEquals(2, twitterFeedProcessor.getAllUsers().get("Ward").getFollowing().size());
    }

    @Test
    public void processTweetsFile() throws IOException {
        BufferedReader bufferedReader = Mockito.mock(BufferedReader.class);
        when(bufferedReader.lines())
                .thenReturn(Stream.of("Alan> If you have a procedure with 10 parameters, you probably missed some.",
                        "Ward> There are only two hard things in Computer Science: cache invalidation, naming things and off-by-1 errors.",
                        "Alan> Random numbers should not be generated with a method chosen at random."));

        twitterFeedProcessor.processTweetsFile("tweet.txt");
        assertFalse(twitterFeedProcessor.getAllUsers().get("Alan").getTweets().isEmpty());
        assertTrue(twitterFeedProcessor.getAllUsers().get("Martin").getTweets().isEmpty());
        assertEquals(2, twitterFeedProcessor.getAllUsers().get("Alan").getTweets().size());
    }

    @Test(expected = RuntimeException.class)
    public void processTweetsFileWithNoUsersLoaded() throws IOException {
        BufferedReader bufferedReader = Mockito.mock(BufferedReader.class);
        when(bufferedReader.lines())
                .thenReturn(Stream.of("Alan> If you have a procedure with 10 parameters, you probably missed some.",
                        "Ward> There are only two hard things in Computer Science: cache invalidation, naming things and off-by-1 errors.",
                        "Alan> Random numbers should not be generated with a method chosen at random."));

        TwitterFeedProcessor incorrectTwitterFeed = new TwitterFeedProcessor(new HashMap<>());
        incorrectTwitterFeed.processTweetsFile("tweet.txt");
    }

    @Test
    public void print() throws IOException {
        BufferedReader bufferedReader = Mockito.mock(BufferedReader.class);
        when(bufferedReader.lines())
                .thenReturn(Stream.of("Alan> If you have a procedure with 10 parameters, you probably missed some.",
                        "Ward> There are only two hard things in Computer Science: cache invalidation, naming things and off-by-1 errors.",
                        "Alan> Random numbers should not be generated with a method chosen at random."));

        twitterFeedProcessor.processTweetsFile("tweet.txt");
        List<String> summary = twitterFeedProcessor.print();
        assertNotNull(summary);
    }

    @Test
    public void getAllUsers() {
        assertNotNull(twitterFeedProcessor.getAllUsers());
    }
}