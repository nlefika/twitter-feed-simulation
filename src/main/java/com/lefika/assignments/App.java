package com.lefika.assignments;

import com.lefika.assignments.processor.TwitterFeedProcessor;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.List;

public class App {
    private static final Logger logger = Logger.getLogger(App.class);

    public static void main(String[] args) {
        try {
            if (args.length != 2) {
                throw new IllegalArgumentException("Application Expecting two input files");
            }

            TwitterFeedProcessor twitterFeed = new TwitterFeedProcessor(new HashMap<>());
            twitterFeed.processUserFile(args[0]);
            twitterFeed.processTweetsFile(args[1]);

            logger.debug(twitterFeed.getAllUsers());

            List<String> feedSummary = twitterFeed.print();

            logger.debug("Tweets Summary" + feedSummary);

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

}
