package com.lefika.assignments.processor;

import java.io.IOException;
import java.util.List;

public interface Processor {
    List<String> process(String fileName) throws IOException;
}
