package com.lefika.assignments.processor;

import com.lefika.assignments.model.User;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;

import static java.util.Comparator.naturalOrder;
import static java.util.Map.Entry.comparingByKey;
import static java.util.stream.Collectors.toList;

@EqualsAndHashCode(callSuper = true)
@Data
public class TwitterFeedProcessor extends FileProcessor {
    private static final Logger logger = Logger.getLogger(TwitterFeedProcessor.class);
    private Map<String, User> allUsers;
    public TwitterFeedProcessor(Map<String, User> allUsers) {
        this.allUsers = allUsers;
    }

    private void saveUser(String line) {
        String[] follows = line.split("follows");
        User user = new User().withName(follows[0].trim()).withFollowing(new HashSet<>()).withTweets(new ArrayList<>());

        for (String name: follows[1].split(",")) {
            User followedUser = new User().withFollowing(new HashSet<>()).withName(name.trim()).withTweets(new ArrayList<>());
            if (!allUsers.containsKey(followedUser.getName())) {
                allUsers.put(followedUser.getName(), followedUser);
            }
            user.getFollowing().add(followedUser.getName());
        }
        allUsers.put(user.getName(), user);
    }

    public void processUserFile(String filename) throws IOException {
        process(filename).forEach(this::saveUser);
    }

    public void processTweetsFile(String filename) throws IOException {
        if (allUsers.isEmpty()) {
            throw new RuntimeException("There are no users to attach to tweets");
        }
        process(filename).forEach(tweet -> {
            String[] tweetPart = tweet.split("> ");
            allUsers.computeIfPresent(tweetPart[0], (k, v) -> {
                v.getTweets().add("@" + tweetPart[0] + ": " + tweetPart[1]);
                return v;
            });
        });
    }

    public List<String> print() {
        return allUsers.entrySet().stream().filter(Objects::nonNull).sorted(comparingByKey())
                .map(getUser()).peek(s -> logger.info(s.getName()))
                .map(updateTweets(allUsers)).filter(noneEmpty())
                .flatMap(Collection::stream)
                .peek(s -> logger.info("\t\t" + s))
                .collect(toList());
    }

    private static Predicate<List<String>> noneEmpty() {
        return s -> !s.isEmpty();
    }

    private static Function<Map.Entry<String, User>, User> getUser() {
        return Map.Entry::getValue;
    }

    private static Function<User, List<String>> updateTweets(Map<String, User> allUser) {
        return s -> allTweets(allUser, s);
    }

    private static List<String> allTweets(Map<String, User> allUser, User s) {
        updateFollowTweets(allUser, s);
        s.getTweets().sort(naturalOrder());
        return s.getTweets();
    }

    private static void updateFollowTweets(Map<String, User> allUser, User s) {
        List<String> networkTweets = s.getFollowing().stream()
                .map(allUser::get).map(User::getTweets).flatMap(Collection::stream).collect(toList());
        s.getTweets().addAll(networkTweets);
    }
}
