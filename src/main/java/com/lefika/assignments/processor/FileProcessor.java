package com.lefika.assignments.processor;

import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class FileProcessor implements Processor{

    public static final String[] REGISTERED_FILES = {"user.txt", "tweet.txt"};
    private static final Logger logger = Logger.getLogger(FileProcessor.class);

    @Override
    public List<String> process(String fileName) throws IOException {
        File infile = new File(fileName);
        if (!Arrays.asList(REGISTERED_FILES).contains(infile.getName())) {
            throw new IllegalArgumentException("Invalid file: File needs to ne in registered list");
        }

        try (BufferedReader br = new BufferedReader(new FileReader(infile))) {
            return br.lines().peek(logger::debug).collect(Collectors.toList());
        }

    }
}
