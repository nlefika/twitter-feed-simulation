package com.lefika.assignments.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Wither;

import java.util.List;
import java.util.Objects;
import java.util.Set;

@Data
@Wither
@AllArgsConstructor
@NoArgsConstructor
public class User {
    String name;
    Set<String> following;
    List<String> tweets;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        User user = (User) o;
        return Objects.equals(name, user.name) &&
                Objects.equals(following, user.following);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, following);
    }
}
